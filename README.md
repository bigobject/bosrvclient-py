# BigObject Service client

This is the RPC client for BigObject Service powered by Apache Thrift.

- [What is BigObject](http://docs.bigobject.io/)
- [Get BigObject on HEROKU](https://addons.heroku.com/bigobject)

# Getting this package

The recommended way to get latest build from this repo is through the official
cheese factory using [pip](https://pip.pypa.io/en/latest/quickstart.html).

```
$ pip install bosrvclient
```

# Basic usage instructions

bosrvclient utilizes HTTP/HTTPS transport and Apache Thrift JSON protocol for
delivering and recving data.

To create a connection helper, start by including **bosrvclient**

```python
from bosrv.request import connect

with connect(<uri_to_bigobject>, <timeout>) as conn:
    token, client = conn
```

A typical query structure can be layout into the following:

- Specify and send your query via execute method
- For methods returning a handle (sha256 string), use cursor helpers to get
  data back
- Data retrieved are encoded as JSON string.

```python
from bosrv.ttypes import *
from exc.ttypes import *

import json

res = client.execute(token, <stmt>, '', '')
eol = None
while eol != -1:
    data = json.loads(client.cursor_fetch(
        token,
        res,
        RangeSpec(page=100)
    ))
    eol, rows = data # if eol == -1, then no more data to read
```

## Race conditions with cursor access

While all operations can be interleaved, *cursor* keep track of access progress
through internal data update.  To avoid race condition, specify *start* to in
**RangeSpec** to take hold of indexing at each *cursor\_fetch*.

# Handling resource references

We strongly urge you to close the returned resource.  While garbage collection
is done routinely on the BigObject service, it intended for unexpected
termination from client application, hence the garbage collection cycle is
kept at a minimal pace.

While it is valid to cache the returned resource handle, make sure your
application handles exceptions accordingly.

# Exceptions produced by API during runtime

## AuthError

produced when providing an invalid authentication token

## ServiceError

general mishaps or signs of really bad server state
