#!/usr/bin/env python

#from bosrv.connect import connect
from bosrv.request import connect
from bosrv.ttypes import *
from exc.ttypes import *

import itertools
import json
import os
import random
import time

users = (
    (1, 'M', 'Taiwan'),
    (2, 'M', 'US'),
    (3, 'F', 'Sleepy Hollow'),
    (4, 'F', 'Taiwan'),
    (5, 'M', 'Japan'),
    (6, 'F', 'United Kindom'),
    (7, 'F', 'Scott'),
    (8, 'F', 'Happy Tree Valley')
)

shoes = (
    (1, 'Toms'),
    (2, 'Sperry'),
    (3, 'Kate'),
    (4, 'Sofi'),
    (5, 'McClain'),
    (6, 'Cherry'),
    (7, 'Miu Miu'),
    (8, 'Salvatore Ferrogamo'),
)

def setup_environ(addr_info):
    with connect(addr_info) as conn:
        token, cli = conn

        sql_stmt = [
            'CREATE TABLE users (id INT, gender STRING, country STRING, KEY (id))',
            'CREATE TABLE shoes (id INT, brand STRING, KEY(id))',
            "CREATE TABLE steps (users.id INT, shoes.id INT, FACT step INT)",
        ]

        for create_stmt in sql_stmt:
            cli.execute(token, create_stmt, '', '')

        insert_stmt = 'INSERT INTO users VALUES ' + ' '.join(map(
            lambda r: "({}, '{}', '{}')".format(*r),
            users
        ))
        cli.execute(token, insert_stmt, '', '')
        del insert_stmt

        insert_stmt = 'INSERT INTO shoes VALUES ' + ' '.join(map(
            lambda r: "({}, '{}')".format(*r),
            shoes
        ))
        cli.execute(token, insert_stmt, '', '')
        del insert_stmt

def populate_steps(addr_info, report=True, count=1):
    with connect(addr_info) as conn:
        token, cli = conn

        def make_tuple(index):
            return (
                users[random.randrange(len(users))][0],
                shoes[random.randrange(len(shoes))][0],
                random.randrange(1, 10)
            )

        now = time.time()

        for _ in xrange(count):
            step_grp_val = 'INSERT INTO steps VALUES ' + ' '.join(map(
                lambda r: "({}, {}, {})".format(*r),
                itertools.imap(make_tuple, xrange(1, 10000))
            ))

            cli.execute(token, step_grp_val, '', '')
            del step_grp_val

        end = time.time()

        if report:
            print '--------------------------'
            print 'Insert time for %d values: %s' % (10000 * count, end - now)
            print '--------------------------\n'

if __name__ == '__main__':
    addr_info = os.environ.get('BIGOBJECT_URL')

    setup_environ(addr_info) # setup the environment
    populate_steps(addr_info, count=1) # setup the environment, for our fact table

    with connect(addr_info, timeout=None) as conn:
        token, cli = conn

        select_stmt = 'SELECT SUM(step) FROM steps GROUP BY users.gender, shoes.brand'
        now = time.time()
        table = cli.execute(token, select_stmt, '', '')
        end = time.time()

        print '--------------------------'
        print select_stmt
        print 'Operation took time: %s' % (end - now)
        print '--------------------------\n'

        eol = limit = 100 # decide the limit of records returned per fetch
        result_table = [] # the destination storage for the result table

        now = time.time()
        while eol != -1:
            chunk = json.loads(cli.cursor_fetch(
                token,
                table,
                RangeSpec(page=limit)
            ))
            eol, rows = chunk
            result_table.extend(rows)
        else:
            cli.cursor_close(token, table)
        end = time.time()

        print '--------------------------'
        print 'Time spent to retrieve from cursor: %s' % (end - now)
        for record in result_table:
            print record
        print '--------------------------\n'
