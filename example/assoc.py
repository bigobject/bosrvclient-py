#!/usr/bin/env python

from bosrv.request import connect
from bosrv.ttypes import *
from exc.ttypes import *

from sql import setup_environ
from sql import populate_steps

import json
import os
import time

if __name__ == '__main__':
    addr_info = os.environ.get('BIGOBJECT_URL')

    setup_environ(addr_info) # setup the environment
    populate_steps(addr_info, count=1) # setup the environment, for our fact table

    with connect(addr_info) as conn:
        token, cli = conn

        '''Setup to run simple association demonstration'''

        build_stmt_qbo = "build association q3(shoes.brand) by users.id from steps"
        now = time.time()
        cli.execute(token, build_stmt_qbo, '', '')
        end = time.time()

        print '--------------------------'
        print build_stmt_qbo
        print 'Operation took time: %s' % (end - now)
        print '--------------------------\n'

        query_stmt = "get 10 freq('Sperry') from q3"
        now = time.time()
        table = cli.execute(token, query_stmt, '', '')
        end = time.time()

        print '--------------------------'
        print query_stmt
        print 'Operation took time: %s' % (end - now)
        print '--------------------------\n'

        eol = limit = 100 # decide the limit of records returned per fetch
        result_table = [] # the destination storage for the result table

        while eol != -1:
            chunk = json.loads(cli.cursor_fetch(
                token,
                table,
                RangeSpec(page=limit)
            ))
            eol, rows = chunk
            result_table.extend(rows)
        else:
            cli.cursor_close(token, table)

        print '--------------------------'
        print 'Time spent to retrieve from cursor: %s' % (end - now)
        for record in result_table:
            print record
        print '--------------------------\n'
